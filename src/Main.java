import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

class MLP {
    private int inputSize; // quantidades de neuronios de entrada
    private int hiddenSize; // quantidades de neuronios da camada oculta
    private int outputSize; // quantidades de neuronios de saida

    private double[][] inputHiddenWeights; // pesos das conexoes de camada de entrada -> camada oculta
    private double[][] hiddenOutputWeights; // matriz de pesos que define as conexões entre a camada oculta e a camada de saída da rede neural

    private double[] hiddenBiases; // bias de entrada p/ oculta
    private double[] outputBiases; // bias de camada oculta p/ saida

    private double learningRate; // taxa de aprendizado do treinamento

    public MLP(int inputSize, int hiddenSize, int outputSize, double learningRate) {
        this.inputSize = inputSize;
        this.hiddenSize = hiddenSize;
        this.outputSize = outputSize;

        this.inputHiddenWeights = new double[inputSize][hiddenSize];
        this.hiddenOutputWeights = new double[hiddenSize][outputSize];

        this.hiddenBiases = new double[hiddenSize];
        this.outputBiases = new double[outputSize];

        this.learningRate = learningRate;

        // Inicializar os pesos das conexoes da camada de entrada p/ oculta aleatoriamente
        Random random = new Random();
        for (int i = 0; i < inputSize; i++) {
            for (int j = 0; j < hiddenSize; j++) {
                inputHiddenWeights[i][j] = random.nextDouble() - 0.5;
            }
        }

        // Inicializar os pesos das conexoes da camada oculta p/ saida aleatoriamente
        for (int i = 0; i < hiddenSize; i++) {
            for (int j = 0; j < outputSize; j++) {
                hiddenOutputWeights[i][j] = random.nextDouble() - 0.5;
            }
        }

        // Inicializar os pesos das bias de entrada p/ oculta aleatoriamente
        for (int i = 0; i < hiddenSize; i++) {
            hiddenBiases[i] = random.nextDouble() - 0.5;
        }

        // Inicializar os pesos das bias da oculta p/ saida aleatoriamente
        for (int i = 0; i < outputSize; i++) {
            outputBiases[i] = random.nextDouble() - 0.5;
        }
    }

    public void train(double[][] input, double[][] output, int epochs) {
        for (int epoch = 0; epoch < epochs; epoch++) {
            System.out.println(epoch);
            for (int i = 0; i < input.length; i++) {
                double[] hiddenOutput = new double[hiddenSize]; // saidas da camada oculta
                double[] networkOutput = new double[outputSize]; // saidas da rede

                // Propagação direta
                for (int j = 0; j < hiddenSize; j++) {
                    // inicia weightedSum com valor de hidden biases
                    double weightedSum = hiddenBiases[j];
                    for (int k = 0; k < inputSize; k++) {
                        // atualiza weightedSum com valor de input * inputHiddenWeights
                        weightedSum += input[i][k] * inputHiddenWeights[k][j];
                    }
                    // Insere no array hiddenOutput posicao j o valor da função de ativação -> sigmoid(weightedSum)
                    hiddenOutput[j] = tanh(weightedSum);
                }

                for (int j = 0; j < outputSize; j++) {
                    // inicia weightedSum com valor de output biases
                    double weightedSum = outputBiases[j];
                    for (int k = 0; k < hiddenSize; k++) {
                        // atualiza weightedSum com valor de hiddenOutput * hiddenOutputWeights
                        // obs: hiddenOutput foi atualizado no for anteiror.
                        weightedSum += hiddenOutput[k] * hiddenOutputWeights[k][j];
                    }
                    // Insere no array networkOutput posicao j o valor de sigmoid(weightedSum)
                    networkOutput[j] = tanh(weightedSum);
                }

                // Propagação reversa
                // cria um array chamado outputError com o mesmo tamanho da camada de saída da rede.
                // O objetivo é calcular o erro entre a saída desejada (que está armazenada no array output) e a
                // saída atual da rede (que foi calculada na linha anterior e está armazenada no array networkOutput).
                // Para isso, é percorrido um loop que itera por cada neurônio da camada de saída,
                // calculando o erro para cada um deles.
                double[] outputError = new double[outputSize];
                for (int j = 0; j < outputSize; j++) {
                    outputError[j] = output[i][j] - networkOutput[j];
                }

                // O cálculo do erro nas camadas ocultas é feito usando o algoritmo de retropropagação de erro,
                // que consiste em propagar o erro da saída da rede de volta para a entrada.
                // Isso é feito multiplicando o erro da camada de saída pelos pesos que
                // conectam a camada de saída à camada oculta.
                double[] hiddenError = new double[hiddenSize];
                // O loop "for" externo percorre cada neurônio na camada oculta,
                // e o loop "for" interno percorre cada neurônio na camada de saída.
                for (int j = 0; j < hiddenSize; j++) {
                    double weightedSum = 0;
                    for (int k = 0; k < outputSize; k++) {
                        weightedSum += outputError[k] * hiddenOutputWeights[j][k];
                    }
                    // atualiza erro da camada de saida
                    // (que é usada posteriormente para ajustar os pesos da camada de entrada à camada oculta.)
                    hiddenError[j] = weightedSum * tanhDerivative(hiddenOutput[j]);
                }

                // Atualização dos pesos e vieses
                for (int j = 0; j < outputSize; j++) {
                    for (int k = 0; k < hiddenSize; k++) {
                        // atualiza pesos de conexões da camada oculta p/ saida
                        hiddenOutputWeights[k][j] += learningRate * outputError[j] * hiddenOutput[k];
                    }
                    // atualiza pesos de conexões bias de camada oculta p/ saida
                    outputBiases[j] += learningRate * outputError[j];
                }

                for (int j = 0; j < hiddenSize; j++) {
                    for (int k = 0; k < inputSize; k++) {
                        // atualiza pesos de conexões da entrada p/ camada oculta
                        inputHiddenWeights[k][j] += learningRate * hiddenError[j] * input[i][k];
                    }
                    // atualiza pesos de conexões bias de entrada p/ camada oculta
                    hiddenBiases[j] += learningRate * hiddenError[j];
                }

                // condição de parada quando erro menor que 0.000003, depois rever esse trecho...
                boolean goodApproximation = true;
                for (int n = 0; i < output.length; i++) {
                    if (networkOutput[n] - output[n][0] > 0.000003) {
                        goodApproximation = false;
                        break;
                    }
                }

                if(goodApproximation) return;
            }
        }
    }

    public double[] predict(double[] input) {
        double[] hiddenOutput = new double[hiddenSize]; // array de tamanho da quantidade de neuronios da camada oculta
        double[] networkOutput = new double[outputSize]; // array de tamanho de neuronios da saida

        // utilizamos os valores de entrada para para produzir
        // o resultado (saida) da camada oculta
        // percorremos os neuronios da camada oculta e calculamos a seguinte função para cada um:
        // hiddenOutput[j] [j] = tanh(input[k] * inputHiddenWeights[k][j]+ hiddenBiases[j]),
        // onde input[k] = valores de entrada,
        // inputHiddenWeights[k][j] = peso de k (neuronio entrada) p/ j (neuronio oculto)
        // hiddenBiases[j] = valor do viés p/ cada neuronio da camada oculto.
        for (int j = 0; j < hiddenSize; j++) {
            double weightedSum = hiddenBiases[j];
            for (int k = 0; k < inputSize; k++) {
                weightedSum += input[k] * inputHiddenWeights[k][j];
            }
            hiddenOutput[j] = tanh(weightedSum);
        }

        // Como anteriormente obtemos as saídas da camada oculta, agora utilizamos os resultados para produzir
        // o resultado (saida) da rede neural.
        // percorremos os neuronios de saida e calculamos a seguinte função para cada um:
        // networkOutput[j] = tanh(hiddenOutput[k] * hiddenOutputWeights[k][j] + outputBiases[j]),
        // onde hiddenOutput[k] = saída calculada no for anterior,
        // hiddenOutputWeights[k][j] = peso de k (neuronio oculto) p/ j (saída)
        // outputBiases[j] = valor do viés p/ cada neuronio de saida.
        for (int j = 0; j < outputSize; j++) {
            double weightedSum = outputBiases[j];
            for (int k = 0; k < hiddenSize; k++) {
                weightedSum += hiddenOutput[k] * hiddenOutputWeights[k][j];
            }
            networkOutput[j] = tanh(weightedSum);
        }

        return networkOutput;
    }

    private double tanh(double x) {
        return Math.tanh(x);
    }

    private double tanhDerivative(double x) {
        double tanhX = Math.tanh(x);
        return 1 - Math.pow(tanhX, 2);
    }

    public static void main(String[] args) {
        // Essa implementação utiliza a função de ativação tanh para as unidades da camada escondida e da camada de saída,
        // e a sua versão derivada para o cálculo do erro durante a propagação reversa. O algoritmo de aprendizado utilizado
        // é o gradiente descendente com atualização dos pesos a cada exemplo apresentado, durante um número fixo de épocas.
        // Note que os pesos e vieses são inicializados aleatoriamente e que o tamanho da camada escondida é um parâmetro que
        // pode ser ajustado de acordo com o problema em questão. Para resolver o problema OR ou XOR, basta substituir a
        // matriz de saída `output` pelos valores esperados correspondentes e ajustar o número de neurônios na camada escondida, se necessário.


        double[][] input = {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}};
        double[][] output = {{-1}, {-1}, {-1}, {1}};

        MLP mlp = new MLP(2, 4, 1, 0.1);
        mlp.train(input, output, 10000);

        DecimalFormat df = new DecimalFormat("#.##########");

        for (int i = 0; i < input.length; i++) {
            System.out.println(Arrays.toString(input[i]) + " = " + df.format(mlp.predict(input[i])[0]));
        }
    }
}